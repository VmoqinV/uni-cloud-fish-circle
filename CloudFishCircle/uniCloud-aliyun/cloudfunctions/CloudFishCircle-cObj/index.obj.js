const db = uniCloud.database()
let dbJQL = null
module.exports = {
	_before: function() {
		dbJQL = uniCloud.databaseForJQL({
			clientInfo: this.getClientInfo()
		})
	},

	get: async function() {
		let artTemp = dbJQL.collection("cloud_fishCircle-article").where(`delState != true`).field(
			"title,user_id,description,picurls,comment_count,like_count,view_count,publish_date").getTemp();
		let userTemp = dbJQL.collection("uni-id-users").field("_id,username,nickname,avatar_file").getTemp();
		return await dbJQL.collection(artTemp, userTemp).limit(3).get()
	},

	/**
	 * 发布文章
	 * @param {object} param1 发布的文章内容
	 * @returns {object} true返回param1, false返回
	 */
	add: function(param1) {
		if (!param1) {
			return {
				errCode: 'PARAM_IS_NULL',
				errMsg: '参数不能为空'
			}
		}

		// const res = await db.collection("cloud_fishCircle-article").add(param1)
		db.collection("cloud_fishCircle-article").add(param1)

		return { //返回值
			param1
		}
	}
}
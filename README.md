# uniCloudFishCircle

#### 介绍

uniCloud多用户交流圈

#### 项目演示

![项目跳转二维码](readmeimage/project_jump.png)

####  使用了什么

- uniApp
- uniCloud
- uni-id

####  实现

##### 登录

![my](readmeimage/login.png)

##### 首页

![index](readmeimage/index.png)

##### 我的

![my](readmeimage/noLoginMy.png)

![my](readmeimage/loginMy.png)

##### 文章详情

![article](readmeimage/articleDetail.png)

![article](readmeimage/articleDetail2.png)

##### 评论详情

![article](readmeimage/commentDetails.png)

##### 发布文章

![edit](readmeimage/editArticle.png)

##### 用户详情

![edit](readmeimage/userDetail.png)

##### 用户文章列表

![edit](readmeimage/userArticleList.png)

##### 用户评论列表

![edit](readmeimage/userCommentLIst.png)

##### 用户点赞列表

![like](readmeimage/userLikeList.png)

##### 反馈

![like](readmeimage/feedback.png)

##### 用户反馈列表

![like](readmeimage/usersFeedbackList.png)

##### 用户详情

![like](readmeimage/feedbackDetail.png)

####  参考

视频教程：https://www.bilibili.com/video/BV1yG4y1h7ck

gitee地址：https://gitee.com/qingnian8/uniall/tree/master/uniCloudRise